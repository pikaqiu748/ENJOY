import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import state from './state.js'
import getters from './getters.js'
import mutations from './mutaitons.js'
import actions from './actions.js'
const store = new Vuex.Store({
	state: state,
	getters: getters,
	mutations: mutations,
	actions: actions
})
export default store
