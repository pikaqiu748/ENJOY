const state = {
	follow: [{
			id: 0,
			username: "碳酸化物",
			userpic: "/static/demo/demo66.jpg",
			newstime: "2020-07-20 下午04:30",
			isFollow: true,
			title: "哈哈哈",
			titlepic: "/static/demo/datapic/1.jpg",
			support: {
				type: "support", // 顶
				support_count: 6,
				unsupport_count: 4,
				click_two: true,
			},
			comment_count: 2,
			share_num: 2
		},
		{
			id: 1,
			username: "动脉影",
			userpic: "/static/demo/demo2.jpg",
			newstime: "2020-07-20 下午04:30",
			isFollow: true,
			title: "嘻嘻嘻",
			titlepic: "/static/demo/datapic/11.jpg",
			support: {
				type: "unsupport", // 踩
				support_count: 3,
				unsupport_count: 4,
				click_two: true,
			},
			comment_count: 2,
			share_num: 2
		},
		{
			id: 2,
			username: "快来看啊",
			userpic: "/static/demo/demo6.jpg",
			newstime: "2020-07-20 下午04:30",
			isFollow: true,
			title: "嘿嘿嘿",
			titlepic: "/static/demo/datapic/19.jpg",
			support: {
				type: "", // 未操作
				support_count: 3,
				unsupport_count: 5,
				click_two: true,
			},
			comment_count: 2,
			share_num: 2
		}
	],

	msg: [{
		disabled: false,
		avatar: "/static/default.jpg",
		username: "昵称1",
		update_time: "1595554336",
		data: "haha",
		noread: 3
	}, {
		disabled: false,
		avatar: "/static/default.jpg",
		username: "昵称2",
		update_time: "1595554336",
		data: "haha",
		noread: 2
	}, {
		disabled: false,
		avatar: "/static/default.jpg",
		username: "昵称3",
		update_time: "1595554336",
		data: "haha",
		noread: 7
	}, {
		disabled: false,
		avatar: "/static/default.jpg",
		username: "昵称4",
		update_time: "1595554336",
		data: "haha",
		noread: 11
	}],

	fans: [{
		avatar: "/static/default.jpg",
		username: "昵称",
		sex: 1, // 0未知，1女性，2男性
		age: 24,
		isFollow: false
	}, {
		avatar: "/static/default.jpg",
		username: "昵称",
		sex: 2, // 0未知，1女性，2男性
		age: 24,
		isFollow: false
	}],
}

export default state;
